# docker-deprem-geolocation

Turkiye'de olusan son depremi InfluxDB'ye ekleyerek, bu depremlerden buyuklugu 4 ve uzerinde olanlari Grafana dashboard uzerinde tablo ve haritali olarak gosterir.

![Grafana Deprem Dashboard](deprem_dashboard.png)

## containeri olusturun. (build)
docker built -t docker-deprem-geolocation .

## environment degiskenlerini ayarlayin.
docker-compose.yml icerisindeki environment degiskenlerini kendinize gore ayarlamalisiniz. Bu amacla ayri bir environment dosyasi da kullanabilirsiniz.

- InfluxDB container için kullanılacak değişkenler

| Degişken | Açıklama |
|------|------|
| INFLUXDB_ADMIN_USER | InfluxDB üzerinde DB oluşturmak için kullanıcı |
| INFLUXDB_ADMIN_PASSWORD | InfluxDB üzerinde DB oluşturmak için kullanıcı parolası |
| INFLUXDB_DB | Kullanıcılacak veritabanı ismi |

- deprem-geolocation container için kullanılacak değişkenler

| Degişken | Açıklama |
|------|------|
| DB_ADDRESS | InfluxDB container ismi |
| DB_PORT | InfluxDB container portu |
| DB_USER | InfluxDB kullanıcı ismi (INFLUXDB_ADMIN_USER ile aynı olmalıdır) |
| DB_PASSWORD | InfluxDB parolası (INFLUXDB_ADMIN_PASSWORD ile aynı olmalıdır) |
| DB_DATABASE | InfluxDB veritabanı ismi (INFLUXDB_DB ile aynı olmalıdır) |
| GOTIFY_ENABLE | Gotify kullanacaksanız "true" |
| GOTIFY_URL | Gotify server URL (https://gotify.example.com) |
| GOTIFY_TOKEN | Gotify App Token |
| MIN_ML | Gotify mesaji gönderilmesi için gerekli en düşük magnitude değeri |


## docker-compose stacki calistirin.
docker-compose up -d

## Grafana dashboardunu olusturun. (import dashboard)
Grafana kurulup ayarlandiktan sonra, paketteki deprem_dashboard.json JSON dosyasini kullanarak import islemi yapin.

