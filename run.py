#!/usr/bin/python3

import time
import json
import os
import requests
from influxdb import InfluxDBClient
from datetime import datetime

# Main settings
DEPREM_URL    = os.environ['DEPREM_URL']
GOTIFY_ENABLE = os.environ['GOTIFY_ENABLE']
MIN_ML        = os.environ['MIN_ML']

# InfluxDB Settings
DB_ADDRESS = os.environ.get('DB_ADDRESS')
DB_PORT = os.environ.get('DB_PORT')
DB_USER = os.environ.get('DB_USER')
DB_PASSWORD = os.environ.get('DB_PASSWORD')
DB_DATABASE = os.environ.get('DB_DATABASE')
DATA_RETRY_INTERVAL = int(os.environ.get('DATA_RETRY_INTERVAL'))

# Create influxdb client
dbclient = InfluxDBClient(
    host=DB_ADDRESS,
    port=DB_PORT,
    username=DB_USER,
    password=DB_PASSWORD,
    database=DB_DATABASE,
)

# return gotify URL if it's enabled as environment variable
def get_gotify_url():
  if GOTIFY_ENABLE =="true":
    gotify_token    = os.environ['GOTIFY_TOKEN']
    gotify_base_url = os.environ['GOTIFY_URL']
    gotify_url      = ("%s/message?token=%s" % (gotify_base_url, gotify_token))

    return gotify_url
  else:
    return False

# send gotify message
def send_gotify_message(gotify_url, title, message):
   resp = requests.post(gotify_url, json={
     "message": message,
     "priority": 1,
     "title": title
   })

# Get last earthquake
def get_last_deprem():
  user_agent = 'deprem exporter - gurcan@gurcanozturk.com'
  request = requests.get(DEPREM_URL, headers={'User-Agent': user_agent})
  son_deprem = json.loads(request.text)["data"][0]
  return son_deprem

# check if we write last deprem before
def is_duplicate(deprem_time, longitude, latitude):
   query  = ("SELECT count(latitude) FROM son_depremler WHERE deprem_time='%s' AND longitude='%s' AND latitude='%s' GROUP BY count FILL(1)" % (deprem_time, longitude, latitude))
   result = dbclient.query(query)
   data   = list(result.get_points())

   if (len(data)) > 0:
     duplicate = True
   else:
     print (data)
     duplicate = False

   return duplicate

# create or switch to correct db
def init_db():
    try:
        databases = dbclient.get_list_database()
    except:
        print ("Error! Unable to get list of databases")
        raise RuntimeError("No DB connection") from error
    else:
        if len(list(filter(lambda x: x['name'] == DB_DATABASE, databases))) == 0:
            dbclient.create_database(DB_DATABASE)
        else:
            dbclient.switch_database(DB_DATABASE)

measurements = []

db_initialized = False

while(db_initialized == False):
    try:
       init_db()  # Setup the database if it does not already exist.
    except:
       print ("Error! DB initialization error")
       time.sleep(int(DATA_RETRY_INTERVAL))
    else:
       print ("DB initialization complete")
       db_initialized = True

while (1):
        son_deprem = get_last_deprem()

        if son_deprem:
            date_string = son_deprem["datetime"]
            deprem_time = datetime.strptime(date_string, '%Y.%m.%d %H:%M:%S')
            dt = deprem_time.isoformat() + 'Z'
            location = son_deprem["location"]
            longitude = son_deprem["geolocation"].split(",")[0]
            latitude  = son_deprem["geolocation"].split(",")[1]
            ml   = float(son_deprem["ml"])
            depth   = float(son_deprem["depth"])
            maplink = ("https://www.openstreetmap.org/#map=12/%s/%s" % (longitude, latitude))

            measurements.append({
                "measurement": "son_depremler",
                "time": dt,
                "fields": {
                    "deprem_time": dt,
                    "location": location,
                    "longitude": longitude,
                    "latitude": latitude,
                    "ml": ml,
                    "depth": depth,
                    "maplink": maplink
                }
            })

            try:
                if measurements:
                   check = is_duplicate(dt, longitude, latitude)
                   if (check is True):
                     print ("Duplicate record found! Do not append!")
                     exit()
                   else:
                     dbclient.write_points(measurements)
                     message = ("%s - %s - %s - %s - %s - %.1f" % (deprem_time, location, longitude, latitude, maplink, ml))
                     print (message)
                     title = ("New earthquake!")
                     print (title)
                     time.sleep(DB_RETRY_INTERVAL)

                     if GOTIFY_ENABLE   == "true" AND ml >= MIN_ML:
                       send_gotify_message(get_gotify_url(), title, message)
                       print ("gotify message gonderildi")
            except:
                time.sleep(DATA_RETRY_INTERVAL)
        else:
          time.sleep(DATA_RETRY_INTERVAL)