FROM alpine:latest

COPY ./run.py /

RUN chmod +x /run.py && \
    apk update && \
    apk --no-cache add \
        curl \
        python3 \
        py3-influxdb \
        py3-msgpack && \
        rm -rf /var/cache/apk/*

ENTRYPOINT ["/run.py"]